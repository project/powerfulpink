POWERFUL PINK THEME (for Drupal 6.x)
===========================================================
Thank you for downloading the Powerful Pink theme for Drupal. I created this theme for the women (and their families) who are fighting breastcancer. Powerful Pink theme has two columns (content and one sidebar on the left), fixed width for 800x600 and up, has a PNG transparency fix for IE6. Tested in Firefox, Seamonkey, Internet Explorer (IE6, IE7, IE8), Safari, Opera, Flock, Chrome and Konqueror. The theme is valid XHTML Strict 1.0, valid CSS 3.1 and valid CSS 2.0


FEATURES
===========================================================
Theme features are:
logo
name
slogan
mission
comment_user_picture
favicon

All of these features need to be enabled in order for them to work. Go to Administer > Themes > Site building > Powerful Pink. Enable the Powerful Pink theme if you haven't done this yet. Configure the Powerful Pink theme. Check all features under Toggle Display and save.

If you would like your own logo, upload it using the file form Upload Logo Image. I've provided a PSD and PNG file containing the default logo (you can find these in the theme folder itself). You may need the Gautami font, which you can find for free on the Internet. But if you would like to use another font, that's fine too. You do not need to use my default logo, just make sure your logo is 240px wide for the best result!

Change the Name, Slogan and Mission under Administer > Site configuration > Site Information.


COPYRIGHT
===========================================================
Feel free to edit the theme to suit your needs but I would really appreciate it if you could keep the links to my portfolio intact. Thanks.


AUTHOR
===========================================================
Rosana Kooymans/Miss Design http://www.missdesign.co.uk


FEEDBACK & DONATIONS
===========================================================
You are free to give feedback on my portfolio through my contact form or otherwise. If you would like to thank me for my work on Powerful Pink, please donate your money to a charity for (breast)cancer in your country.